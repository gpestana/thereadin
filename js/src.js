'use strict';

var prod = true;
var errEmail = '';
var errFilter = '';

/* 
 *
	Helper functions
 *
 */

var getCookie = function(name) {
	var v = document.cookie.match('(^|;) ?' + name + '=([^;]*)(;|$)');
	return v ? v[2] : null;
};

var setCookie = function(name, value, days) {
	var d = new Date;
	d.setTime(d.getTime() + 24*60*60*1000*days);
	document.cookie = name + "=" + value + ";path=/;expires=" + d.toGMTString();
};


/* 
 * Lib
 *
 */

var emailValidator = function(email) {
	if(!email) return 0;
	var emailPattern = /^(([^<>()\[\]\\.,;:\s@"]+(\.[^<>()\[\]\\.,;:\s@"]+)*)|(".+"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/; 
	return email.match(emailPattern)
};

var subscribe = function(event) {
	event.preventDefault();
	console.log(email)
	if(errFilter) errFilter.remove();
	if(errEmail) errEmail.remove();

	var input = { 
		contentType: document.getElementById("contentType").value,
		filter: document.getElementById("filter").value,
		email: document.getElementById("email").value
	};

	if(!emailValidator(input.email)) {
		errEmail = $("<p>Invalid email</p>");
		errEmail.appendTo(".error");
	};

	if(!input.filter) {
		errFilter = $("<p>Enter an interest</p>");
		errFilter.appendTo(".error");
	};

	if(!input.filter || !emailValidator(input.email)) return 0;

	setCookie('email', input.email, 5);
	setCookie('filter', input.filter, 5);
	setCookie('contentType', input.contentType, 5)

	window.location.href = 'successful_subs.html';
};

var goToMainPage = function(event) {
	event.preventDefault();
	console.log("GO TO MAIN PAGE")
	window.location.href = 'index.html';
};

var showSubscription = function() {
	var filter = getCookie('filter');
	var resultSubscription = 
			$(`<p>Ups, something wrong has happened..</p>`);

	if(filter) {
		resultSubscription = 
			$(`<p>You will receive daily emails about ${filter} in your mailbox</p>`);
	};
	resultSubscription.appendTo(".subscriptionSuccessful");
};

var sendEmail = function() {
	var subscription = {
		email: getCookie('email'),
		filter: getCookie('filter'),
		contentType: getCookie('contentType')
	};

	if(prod) {
		emailjs.send(
			"default_service",
			"default",
			subscription
		);
	} else console.log(subscription);
};



/* 
 * Auto run based on page 
 *
 */

var currentPage = window.location.pathname.split('/').pop();

if(currentPage == 'index.html') {
	var email = getCookie('email');
	if(email) {
		$("#email").attr('placeholder', email);
		$("#email").attr('value', email);
		console.log($("#email"))
	}; 
};

if(currentPage == 'successful_subs.html') {
	(function(){
		emailjs.init("user_3L3vUcR7sQ9kuVHLC8kqe");
	})();

	showSubscription();
	sendEmail();
};

